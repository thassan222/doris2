import axios from 'axios'

axios.defaults.baseURL = 'http://35.176.105.184:5000/'
axios.defaults.method = 'get'
axios.defaults.headers.common['Authorization'] =
  localStorage.getItem('token') || null
axios.defaults.headers.post['Content-Type'] = 'application/json'

export default axios
